﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Security;

namespace WebApi2Demo.Controllers
{
    public class UsersController : ApiController
    {
        public string Get()
        {

            var cookie = Request.Headers.GetCookies();
            FormsAuthenticationTicket ticket = null;

            foreach (var perCookie in cookie[0].Cookies)
            {
                if (perCookie.Name == FormsAuthentication.FormsCookieName)
                {
                    ticket = FormsAuthentication.Decrypt(perCookie.Value);
                    break;
                }
            }
            if (ticket == null)
            {
                return null;
            }
            return ticket.Name;
        }
    }
}
